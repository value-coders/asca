$(document).ready(function () {
  // $(".side-bar-menu li a").click(function () {
  //   $(this).parent().addClass('active').siblings().removeClass('active');

  // });


  $('.acc-head').click(function () {
    $(this).next().slideToggle(500);
    $(this).toggleClass('active');
  })


  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
        $('#imagePreview').hide();
        $('#imagePreview').fadeIn(650);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#imageUpload").change(function () {
    readURL(this);
  });




  var ccheck = $("main").hasClass("popup");
  var cchecktow = $("main select").hasClass("chosen-select");
  var ccheckthree = $("main .sign-up-form-section").hasClass("mem-app");



  if (ccheckthree) {
    var modal = document.querySelector(".modal-wrapper");
    var trigger = document.querySelector(".trigger");
    var closeButton = document.querySelector(".close-button");

    function toggleModal() {
      modal.classList.toggle("show-modal");
    }

    function windowOnClick(event) {
      if (event.target === modal) {
        toggleModal();
      }
    }

    trigger.addEventListener("click", toggleModal);
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);

  }


  else if (ccheck || cchecktow) {


  }


  else {

    var modal = document.querySelector(".modal-wrapper");
    var trigger = document.querySelector(".trigger");
    var closeButton = document.querySelector(".close-button");

    function toggleModal() {
      modal.classList.toggle("show-modal");
    }

    function windowOnClick(event) {
      if (event.target === modal) {
        toggleModal();
      }
    }

    trigger.addEventListener("click", toggleModal);
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);
  }




  $(".chosen-select").chosen({
    width: "100%",
    allow_single_deselect: true,
    allow_single_deselect: true
  });


  // Show the first tab and hide the rest
  $('#tabs-nav li:first-child').addClass('active');
  $('.tab-content').hide();
  $('.tab-content:first').show();

  // Click function
  $('#tabs-nav li').click(function () {
    $('#tabs-nav li').removeClass('active');
    $(this).addClass('active');
    $('.tab-content').hide();

    var activeTab = $(this).find('a').attr('href');
    $(activeTab).fadeIn();
    return false;
  });


  $(".e-user").click(function () {
    $(".edit-user").fadeIn();
  });

  $(".v-user").click(function () {
    $(".m-user").fadeIn();
  });



  $(".del").click(function () {
    $(".del-modal").fadeIn();
  });


  $(".out").click(function () {
    $(".log-out").fadeIn();
  });





  $(".m-btn").click(function () {
    $(".modal-wrappernew").fadeOut();
  });







});
